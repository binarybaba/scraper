var express = require('express'),
    bodyParser = require('body-parser'),
    j2x = require('json2xls'),
    fs = require('fs'),
    osmosis = require('osmosis'),
    app = express(),
    //IP = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1',
    PORT = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;


    app.use(express.static('public'));
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());

    app.get('/', function(req, res){
        res.send('index.html');
    })

    app.post('/scrape', function(req, res){
        var stack=[];
        osmosis.get("https://www.bankbazaar.com/"+req.body.loan+"-loan.html")
            .find('.js-offers-wrapper')
            .set({
                'name': 'span.js-title.js-format-string',
                'interestRange':'div.column-center > span',
                'processingFee': 'div.offer-section-column.col-same-height.col-middle.processing-fee-range',
                'loanAmount': 'div.offer-section-column.col-same-height.col-middle.loan-amount-range',
                'tenureRange': 'div.offer-section-column.col-same-height.col-middle.tenure-range',
            })
            .data(function(results){
                stack.push(results);
            })
            .done(function(){
                var filename = 'bb-'+req.body.loan+'-loan.xlsx';
                fs.writeFileSync(filename, j2x(stack), 'binary');
                res.download(__dirname+'/'+filename, function(err) {
                    if(err){
                        console.log(err);
                        res.status(err.status).end();
                    }
                    else{
                        console.log('Sent file: '+filename);
                    }
                })

            })
    });




/*app.listen(PORT, IP, function(){
    console.log('listening at port ',PORT,' on ',IP);
})*/
app.listen(PORT, function(){
    console.log('listening at port ',PORT);
})